package com.slawacoste.app;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */

    /*https://stackoverflow.com/questions/46931267/how-to-parameterize-with-string-arrays-in-junit-5*/
    static Stream<Arguments> valuesForMultiplication() {
        return Stream.of(Arguments.of((Object) new String[]{"2.0", "2", "4.0"}),
                Arguments.of((Object) new String[]{"50000000000", "3", "150000000000"}),
                Arguments.of((Object) new String[]{"121", "4", "484"}));
    }

    static Stream<Arguments> inputValues() {
        return Stream.of(Arguments.of((Object) new String[]{"2.0", "6", "1", "0"}),
                Arguments.of((Object) new String[]{"2.0", "6", "2", "5"}),
                Arguments.of((Object) new String[]{"2.0", "6", "3", "8"}),
                Arguments.of((Object) new String[]{"2.0", "6", "4", "10"}),
                Arguments.of((Object) new String[]{"2.0", "6", "5", "12"}),
                Arguments.of((Object) new String[]{"2.0", "0", "4", "0"}));
    }

    static void init(String filename) throws App.IncrementException, IOException, NullPointerException {
        Properties property = new Properties();
        FileInputStream fis = new FileInputStream(filename);
        property.load(fis);

        String increment = property.getProperty("increment");
        if (increment.equals("0")) throw new App.IncrementException("Increment is equal to zero");
    }

    @ParameterizedTest
    @MethodSource(value = "valuesForMultiplication")
    void testMultiplyingWithinMultiplyAndPrintMethod(String[] data) {
        String m1 = data[0];
        String m2 = data[1];
        String expected = data[2];
        assertEquals(expected, new BigDecimal(m1).multiply(new BigDecimal(m2)).toString());
    }

    @ParameterizedTest
    @MethodSource(value = "inputValues")
    void testIncrementingWithinMultiplyAndPrintMethod(String[] data) {
        String[] incrementedFactors = {"2.0", "3.0", "4.0", "5.0", "6.0", "2.0", "4.0", "6.0",
                "2.0", "5.0", "2.0", "6.0", "2.0"};
        BigDecimal minimum = new BigDecimal(data[0]);
        BigDecimal maximum = new BigDecimal(data[1]);
        BigDecimal increment = new BigDecimal(data[2]);
        int count = Integer.parseInt(data[3]);
        BigDecimal firstFactor = minimum;
        while (firstFactor.max(maximum).equals(maximum)) {
            BigDecimal expected = new BigDecimal(incrementedFactors[count]);
            assertEquals(expected, firstFactor);
            firstFactor = firstFactor.add(increment);
            count++;
        }
    }

    @Test
    void IncrementExceptionTesting() {
        Throwable exception = assertThrows(App.IncrementException.class,
                () -> init("src/test/resources/testProp.properties"));
        assertEquals("Increment is equal to zero", exception.getMessage());
    }

    @Test
    void IOExceptionTesting() {
        Throwable exception = assertThrows(IOException.class,
                () -> init("testProp.properties"));
        assertEquals("testProp.properties (Не удается найти указанный файл)", exception.getMessage());
    }

    @Test
    void NullPointerExceptionTesting() {
        Throwable exception = assertThrows(NullPointerException.class,
                () -> init("src/test/resources/testEmptyProp.properties"));
        assertEquals("Cannot invoke \"String.equals(Object)\" because \"increment\" is null", exception.getMessage());
    }

}
