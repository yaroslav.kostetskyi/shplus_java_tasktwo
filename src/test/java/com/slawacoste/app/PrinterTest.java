package com.slawacoste.app;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PrinterTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final BigDecimal firstFactor = new BigDecimal("2.0");
    private BigDecimal secondFactor = new BigDecimal("3000000");
    private BigDecimal result = new BigDecimal("6000000");


    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
       }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void testDoublePrint() {
        DoublePrinter doublePrinter = new DoublePrinter();
            doublePrinter.print(firstFactor, secondFactor, result);
            String expected = "2.0 x 3000000.0 = 6000000.0" + System.lineSeparator();
            assertEquals(expected, outContent.toString());
        }

    @Test
    void testIntegerPrint() {
        IntegerPrinter integerPrinter = new IntegerPrinter();
        integerPrinter.print(firstFactor, secondFactor, result);
        String expected = "2 x 3000000 = 6000000" + System.lineSeparator();
        assertEquals(expected, outContent.toString());
    }

    @Test
    void testFloatPrint() {
        FloatPrinter floatPrinter = new FloatPrinter();
        floatPrinter.print(firstFactor, secondFactor, result);
        String expected = "2.0 x 3000000.0 = 6000000.0" + System.lineSeparator();
        assertEquals(expected, outContent.toString());
    }

    @Test
    void testLongPrint() {
        LongPrinter longPrinter = new LongPrinter();
        longPrinter.print(firstFactor, secondFactor, result);
        String expected = "2 x 3000000 = 6000000" + System.lineSeparator();
        assertEquals(expected, outContent.toString());
    }

}