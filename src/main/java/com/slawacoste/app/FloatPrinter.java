package com.slawacoste.app;

import java.math.BigDecimal;

public class FloatPrinter implements Printer {

    public void print(BigDecimal firstFactor, BigDecimal secondFactor, BigDecimal result) {
        System.out.println(firstFactor.floatValue() + " x " + secondFactor.floatValue() +
                " = " + result.floatValue());
    }
}
