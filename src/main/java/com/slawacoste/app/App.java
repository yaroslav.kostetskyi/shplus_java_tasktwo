package com.slawacoste.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;
import java.util.Scanner;

import static com.slawacoste.app.MyConstants.FILENAME;

/**
 * Hello world!
 */
public class App {
    private static String minimum;
    private static String maximum;
    private static String increment;
    private static String type;
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        init();
        if (type != null) {
            logger.info("Type is given: {}", type);
            printIfTypeGiven();

        } else {
            logger.info("Type is not given. Prints with type 'int' by default");
            printByDefault();
        }
        
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

    }

    private static void init() {
        Properties property = new Properties();
        try (FileInputStream fis = new FileInputStream(FILENAME)) {
            property.load(fis);
            logger.trace("Load property file into the Property object");
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("IOException while loading property file", e);
        }
        try {
            minimum = property.getProperty("min");
            logger.info("Get min value from property: {}", minimum);
            maximum = property.getProperty("max");
            logger.info("Get maximum value from property: {}", maximum);
            increment = property.getProperty("increment");
            logger.info("Get increment value from property: {}", increment);
            if (increment.equals("0")) throw new IncrementException("Increment is equal to zero");
        } catch (NullPointerException e) {
            e.printStackTrace();
            logger.error("NullPointerException while getting values from property", e);
        } catch (IncrementException e) {
            e.printStackTrace();
            logger.error("IncrementException: Increment is equal to zero", e);
        }

        type = System.getProperty("type");
        logger.info("Get type value from the system variable: {}", type);
    }

    private static void multiplyAndPrint(Printer printer) {
        BigDecimal min = new BigDecimal(minimum);
        BigDecimal max = new BigDecimal(maximum);
        BigDecimal incr = new BigDecimal(increment);
        logger.trace("Created BigDecimal values: min={}, max={}, incr={}", min, max, incr);
        BigDecimal firstFactor = min;
        BigDecimal secondFactor;
        BigDecimal result;
        logger.trace("Start to multiply");
        while (firstFactor.max(max).equals(max)) {
            secondFactor = min;
            logger.info("Multiply by firstFactor = {}", firstFactor);
            while (secondFactor.max(max).equals(max)) {
                result = firstFactor.multiply(secondFactor);
                logger.trace("Calculate multiplying firstFactor: {} and secondFactor: {}. Result = {}", firstFactor,
                        secondFactor, result);
                printer.print(firstFactor, secondFactor, result);
                secondFactor = secondFactor.add(incr);
                logger.trace("Increment secondFactor");
            }
            System.out.println("-------------------");
            firstFactor = firstFactor.add(incr);
            logger.trace("Increment firstFactor");
        }
    }

    private static void printIfTypeGiven() {
        Printer printer;
        switch (type) {
            case ("double"):
                printer = new DoublePrinter();
                multiplyAndPrint(printer);
                logger.trace("Done (multiplyAndPrint) with double");
                break;
            case ("long"):
                printer = new LongPrinter();
                multiplyAndPrint(printer);
                logger.trace("Done (multiplyAndPrint) with long");
                break;
            case ("float"):
                printer = new FloatPrinter();
                multiplyAndPrint(printer);
                logger.trace("Done (multiplyAndPrint) with float");
                break;
            default:
                printByDefault();
                logger.trace("Done (multiplyAndPrint) by default");
                break;
        }
    }

    private static void printByDefault() {
        Printer printer = new IntegerPrinter();
        multiplyAndPrint(printer);
        logger.trace("Done (multiplyAndPrint) by default");
    }

    /**
     * The class that provides an exception if an increment is equal to zero.
     */
    static class IncrementException extends Exception {
        public IncrementException(String exceptionMessage) {
            super(exceptionMessage);
        }
    }

}
