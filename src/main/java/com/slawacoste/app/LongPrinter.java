package com.slawacoste.app;

import java.math.BigDecimal;

public class LongPrinter implements Printer {

    public void print(BigDecimal firstFactor, BigDecimal secondFactor, BigDecimal result) {
        System.out.println(firstFactor.longValue() + " x " + secondFactor.longValue() +
                " = " + result.longValue());
    }

}
