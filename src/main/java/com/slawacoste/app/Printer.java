package com.slawacoste.app;

import java.math.BigDecimal;

public interface Printer {

    void print(BigDecimal firstFactor, BigDecimal secondFactor, BigDecimal result);

}
