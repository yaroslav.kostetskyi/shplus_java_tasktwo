package com.slawacoste.app;

import java.math.BigDecimal;

public class IntegerPrinter implements Printer {

    public void print(BigDecimal firstFactor, BigDecimal secondFactor, BigDecimal result) {
        System.out.println(firstFactor.intValue() + " x " + secondFactor.intValue() +
                " = " + result.intValue());
    }
}
