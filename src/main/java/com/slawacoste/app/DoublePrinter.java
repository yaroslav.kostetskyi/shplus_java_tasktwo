package com.slawacoste.app;

import java.math.BigDecimal;

public class DoublePrinter implements Printer {

    public void print(BigDecimal firstFactor, BigDecimal secondFactor, BigDecimal result) {
        System.out.println(firstFactor.doubleValue() + " x " + secondFactor.doubleValue() +
                " = " + result.doubleValue());

    }
}
